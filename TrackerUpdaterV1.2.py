import pandas as pd
import datetime
import subprocess
import time
import threading
import json
import requests
import http
from requests.auth import HTTPDigestAuth
from kivy.app import App
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.lang import Builder
from kivy.properties import StringProperty
import keyboard
import numpy
import schedule

# Set default window size #
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '600')
Config.set('graphics', 'height', '700')

# Creating local lists of IPs and Serials #
onlineIp = []
onlinePSU = []
offlineIp = []
onlineSerial = []
offlineSerial = []
openIp = []
offlinePSU = []


def localList():

    # Opening Online Tracker and Offline Tracker #
    ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
    offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
    psut = pd.read_csv(r'//10.20.0.25/Documents/offline PSU tracker.csv', dtype=object)

    for ip in ont['IP Address']:
        onlineIp.append(ip)

    for ip in offt['IP Address']:
        offlineIp.append(ip)

    for serial in ont['serialNumber']:
        onlineSerial.append(serial)

    for serial in offt['serialNumber']:
        offlineSerial.append(serial)

    for serial in ont['PSU SN']:
        onlinePSU.append(serial)

    for serial in psut['serialNumber']:
        offlinePSU.append(serial)


# ALERT WINDOWS #
class noip(Popup):
    pass


class serialnotfound(Popup):
    pass


class serialinCOW(Popup):
    pass


class serialnotinTracker(Popup):
    pass


class noserialEntered(Popup):
    pass


class alreadyDeployed(Popup):
    pass


class snExists(Popup):
    pass


class cleaningoutConf(Popup):
    pass


class cleaninginConf(Popup):
    pass


class error254(Popup):
    pass


class errorRange(Popup):
    pass


class no4c(Popup):
    pass


class notDeployed(Popup):
    pass


class badRow(Popup):
    pass


class noUser(Popup):
    pass


class invalidIP(Popup):
    pass


class nopsu(Popup):
    pass


class nospeed(Popup):
    pass


class nospower(Popup):
    power = ''
    def powerF(self, ip):
        nospower.power = ip
        print(TestApp.nm.ip)
        return ip


class scanfirst(Popup):
    pass


class statuserror(Popup):
    pass


class newpsu(Popup):

    # Function to add new PSU SN to replace RMA'd PSU in tracker #
    def newpsusn(self, newsn, row):

        try:
            ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
            offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
            ont.at[row, 'PSU SN'] = newsn.upper()

            ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
            result = True
            RMA().updateLabel(result)

        except Exception as e:
            result = False
            RMA().updateLabel(result)

    def refocus(self):
        keyboard.press_and_release('enter')
        return


# Fetching GUI widgets and settings #
class updater(BoxLayout):

    # Alert for serial already listed in offline sheet #
    def alert1(self):
        a1 = serialinCOW()
        a1.open()

    # Alert for serial not listed in ontracker #
    def alert2(self):
        a2 = serialnotinTracker()
        a2.open()

    # Alert for no serial number entered #
    def alert3(self):
        a3 = noserialEntered()
        a3.open()

    def nospeed(self):
        nospeed().open()

    # Alert for signaling completion #
    def alreadyDeployed(self):
        d = alreadyDeployed()
        d.open()

    def notDeployed(self):
        n = notDeployed()
        n.open()

    # Alert for serial not found #
    def serialnotfound(self):
        snf = serialnotfound()
        snf.open()

    def noip(self):
        noip().open()

    def snExists(self):
        ip = snExists()
        ip.open()

    def conf(self):
        conf = cleaningoutConf()
        conf.open()

    def conf2(self):
        conf2 = cleaninginConf()
        conf2.open()

    def error254(self):
        error254().open()

    def errorRange(self):
        errorRange().open()

    def no4c(self):
        no4c().open()

    def badRow(self):
        badRow().open()

    def noUser(self):
        noUser().open()

    def invalidIP(self):
        invalidIP().open()

    def nopsu(self):
        nopsu().open()

    def newpsu(self):
        newpsu().open()

    def nospower(self):
        nospower().open()

    def statuserror(self):
        statuserror().open()

    def scanfirst(self):
        scanfirst().open()

# SCREENS #
class WindowManager(ScreenManager):
    pass


class Menu(Screen):

    def checkUser(self, user, nextScreen):
        if user == 'User':
            updater().noUser()
            return
        elif user != 'User':
            self.manager.current = nextScreen
    pass


class refocusRepair(Screen):
    pass


class refocusCOW(Screen):
    pass


class refocusRMA(Screen):
    pass


class refocusHOS(Screen):
    pass


class refocusEXIT(Screen):
    pass


class refocusCI(Screen):
    pass


class refocusCIIP(Screen):
    pass


class refocusCO(Screen):
    pass


class cleaning2(Screen):
    pass


class checkStatus(Screen):

    # Empty variables that will be changed to information specified by variable name #
    network = ''
    serial = ''
    ip = ''  # Used for displaying system info in label below text inputs
    ip1 = ''  # Used for returning IP address in 'IP' text input for easy copy/paste
    type = ''
    typeDisplay = ''
    hashrate = ''
    uptime = ''
    status = ''
    rma = ''
    expiry = ''
    deploydate = ''
    nos = ''
    nosdate = ''
    acc = ''
    psu = ''
    speed = ''
    host = ''
    coin = ''
    pool = ''
    row = 0
    scanned = False

    def display_time(self, seconds, granularity=4):
        result = []
        intervals = (
            ('weeks', 604800),  # 60 * 60 * 24 * 7
            ('days', 86400),  # 60 * 60 * 24
            ('hours', 3600),  # 60 * 60
            ('minutes', 60),
            ('seconds', 1),
        )

        for name, count in intervals:
            value = seconds // count
            if value:
                seconds -= value * count
                if value == 1:
                    name = name.rstrip('s')
                result.append("{} {}".format(value, name))
        return ', '.join(result[:granularity])

    def checkStatusSERIAL(self, scan):

        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt1 = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)
        offt = offt1.replace(numpy.nan, '', regex=True)
        localList()
        scan = scan.upper()

        # If serial field is empty #
        if scan == "":
            updater().alert3()
            return

        # If serial number is in the online tracker #
        # if scan in ont['serialNumber']:

        row = -1
        for x in ont['serialNumber']:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1
                checkStatus.row = row
                ping = str(ont.at[row, 'IP Address'])
                p1 = subprocess.Popen(['ping ', "-n", "1", ping], stdout=subprocess.PIPE)
                p1.wait()

                if p1.poll():
                    #OFFLINE
                    print('offline')
                    checkStatus.network = '[color=ff00ff]Offline[/color]'
                else:
                    #ONLINE
                    print('online')
                    checkStatus.network = '[color=ff00ff]Online[/color]'
                    try:
                        s = requests.Session()
                        url = 'http://' + ping + '/cgi-bin/get_miner_status.cgi'
                        r1 = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                        data1 = json.loads(r1.text)
                        checkStatus.hashrate = '[color=ff00ff]' + str(data1['summary']['ghs5s']) + ' GH/s[/color]'
                        seconds = int(data1['summary']['elapsed'])
                        checkStatus.uptime = '[color=f00ff]' + checkStatus.display_time(self, seconds) + '[/color]'

                    except Exception as e:
                        checkStatus.hashrate = '[color=ff00ff]n/a[/color]'
                        checkStatus.uptime = '[color=ff00ff]n/a[/color]'
                        print(str(e))

                # Fetching information and storing as variable to send to GUI #
                if str(ont.at[row, 'BaseSpeed']) != '':
                    checkStatus.typeDisplay = '[color=ff00ff]' + str(ont.at[row, 'machineType'])+' - '+str(ont.at[row, 'BaseSpeed']) + 'TH[/color]'
                elif str(ont.at[row, 'BaseSpeed']) == '':
                    checkStatus.typeDisplay = '[color=ff00ff]' + str(ont.at[row, 'machineType']) + '[/color]'

                # Fetching information and storing as variable to send to GUI #
                if str(ont.at[row, 'RMA?']) == '' or '0':
                    checkStatus.rma = '[color=ff00ff]0[/color]'
                elif str(ont.at[row, 'RMA?']) != '':
                    checkStatus.rma = '[color=ff00ff]' + str(ont.at[row, 'RMA?']) + '[/color]'
                checkStatus.type = '[color=ff00ff]' + str(ont.at[row, 'machineType']) + '[/color]'
                checkStatus.expiry = '[color=ff00ff]' + str(ont.at[row, 'expiryDate']) + '[/color]'
                checkStatus.psu = '[color=ff00ff]' + str(ont.at[row, 'PSU SN']) + '[/color]'
                checkStatus.speed = '[color=ff00ff]' + str(ont.at[row, 'BaseSpeed']) + '[/color]'
                checkStatus.serial = '[color=ff00ff]' + str(ont.at[row, 'serialNumber']) + '[/color]'
                checkStatus.ip = '[color=ff00ff]' + str(ont.at[row, 'IP Address']) + '[/color]'
                checkStatus.ip1 = str(ont.at[row, 'IP Address'])
                checkStatus.status = '[color=ff00ff]' + str(ont.at[row, 'Status']) + '[/color]'
                checkStatus.deploydate = '[color=ff00ff]' + str(ont.at[row, 'deployDate']) + '[/color]'
                checkStatus.acc = '[color=ff00ff]' + str(ont.at[row, 'accountName']) + '[/color]'
                checkStatus.host = '[color=ff00ff]' + str(ont.at[row, 'Hosted?']) + '[/color]'
                checkStatus.coin = '[color=ff00ff]' + str(ont.at[row, 'coinMined']) + '[/color]'
                checkStatus.pool = '[color=ff00ff]' + str(ont.at[row, 'pool']) + '[/color]'
                checkStatus.nosdate = ''
                checkStatus.nos = ''
                checkStatus.scanned = True
                return

        # If the serial number is in the offline tracker #
        # elif scan not in onlineSerial:

        row = -1
        for x in offt['serialNumber']:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1
                checkStatus.row = row
                # Fetching information and storing as variable to send to GUI #
                if str(ont.at[row, 'BaseSpeed']) != '':
                    checkStatus.typeDisplay = '[color=ff00ff]' + str(offt.at[row, 'machineType'])+' - '+str(ont.at[row, 'BaseSpeed']) + 'TH[/color]'
                elif str(ont.at[row, 'BaseSpeed']) == '':
                    checkStatus.typeDisplay = '[color=ff00ff]' + str(offt.at[row, 'machineType']) + '[/color]'

                # Fetching information and storing as variable to send to GUI #
                if str(offt.at[row, 'RMA?']) == '' or '0':
                    checkStatus.rma = '[color=ff00ff]0[/color]'
                elif str(offt.at[row, 'RMA?']) != '':
                    checkStatus.rma = '[color=ff00ff]' + str(offt.at[row, 'RMA?']) + '[/color]'
                checkStatus.network = '[color=ff00ff]NOS[/color]'
                checkStatus.type = '[color=ff00ff]' + str(offt.at[row, 'machineType']) + '[/color]'
                checkStatus.expiry = '[color=ff00ff]' + str(offt.at[row, 'expiryDate']) + '[/color]'
                checkStatus.psu = '[color=ff00ff]' + str(offt.at[row, 'PSU SN']) + '[/color]'
                checkStatus.speed = '[color=ff00ff]' + str(offt.at[row, 'BaseSpeed']) + '[/color]'
                checkStatus.serial = '[color=ff00ff]' + str(offt.at[row, 'serialNumber']) + '[/color]'
                checkStatus.ip = '[color=ff00ff]' + str(offt.at[row, 'IP Address']) + '[/color]'
                checkStatus.ip1 = str(offt.at[row, 'IP Address'])
                checkStatus.status = '[color=ff00ff]' + str(offt.at[row, 'Status']) + '[/color]'
                checkStatus.deploydate = '[color=ff00ff]' + str(offt.at[row, 'deployDate']) + '[/color]'
                checkStatus.acc = '[color=ff00ff]' + str(offt.at[row, 'accountName']) + '[/color]'
                checkStatus.host = '[color=ff00ff]' + str(offt.at[row, 'Hosted?']) + '[/color]'
                checkStatus.coin = '[color=ff00ff]' + str(offt.at[row, 'coinMined']) + '[/color]'
                checkStatus.pool = '[color=ff00ff]' + str(offt.at[row, 'pool']) + '[/color]'
                checkStatus.nosdate = '[color=ff00ff]' + str(offt.at[row, 'NOS Date']) + ' - ' \
                                      + str(offt.at[row, 'NOS Reason']) + ' - ' + str(offt.at[row, 'Name']) + '[/color]'
                # checkStatus.nos = '[color=ff00ff]' + str(offt.at[row, 'NOS Reason']) + '[/color]'
                checkStatus.scanned = True
                return

        if scan not in onlineSerial or offlineSerial:
            checkStatus.ip = ''
            checkStatus.type = ''
            checkStatus.status = ''
            checkStatus.deploydate = ''
            checkStatus.acc = ''
            checkStatus.host = ''
            checkStatus.coin = ''
            checkStatus.pool = ''
            checkStatus.nosdate = ''
            checkStatus.nos = ''
            checkStatus.hashrate = ''
            checkStatus.uptime = ''
            updater().serialnotfound()
            return

    def checkStatusIP(self, ip):
        scan = ip
        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt1 = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)
        offt = offt1.replace(numpy.nan, '', regex=True)
        localList()
        # If serial field is empty #
        if scan == "":
            updater().alert3()
            return

        if '10.10.' not in scan:
            scan = '10.10.' + scan

        # If serial number is in the online tracker #
        if scan in onlineIp:

            row = -1
            for x in onlineIp:
                if scan != x:
                    row += 1
                elif scan == x:
                    row += 1
                    checkStatus.row = row
                    if str(ont.at[row, 'Status']) == 'On Shelf':
                        ping = str(ont.at[row, 'IP Address'])
                        p1 = subprocess.Popen(['ping ', "-n", "1", ping], stdout=subprocess.PIPE)
                        p1.wait()
                        if p1.poll():
                            checkStatus.network = '[color=ff00ff]Offline[/color]'
                            checkStatus.hashrate = ''
                            checkStatus.uptime = ''
                        else:
                            checkStatus.network = '[color=ff00ff]Online[/color]'
                            try:
                                s = requests.Session()
                                url = 'http://' + ping + '/cgi-bin/get_miner_status.cgi'
                                r1 = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=3)
                                data1 = json.loads(r1.text)
                                checkStatus.hashrate = '[color=ff00ff]' + str(data1['summary']['ghs5s']) + ' GH/s[/color]'
                                seconds = int(data1['summary']['elapsed'])
                                checkStatus.uptime = '[color=ff00ff]' + checkStatus.display_time(self, seconds) + '[/color]'

                            except Exception as e:
                                checkStatus.hashrate = '[color=ff00ff]n/a[/color]'
                                checkStatus.uptime = '[color=ff00ff]n/a[/color]'
                                print(str(e))
                    else:
                        checkStatus.network = '[color=ff00ff]Offline[/color]'
                        checkStatus.hashrate = ''
                        checkStatus.uptime = ''

                    if str(ont.at[row, 'BaseSpeed']) != '':
                        checkStatus.typeDisplay = '[color=ff00ff]' + str(ont.at[row, 'machineType'])+' - '+str(ont.at[row, 'BaseSpeed'])+ 'TH[/color]'
                    elif str(ont.at[row, 'BaseSpeed']) == '':
                        checkStatus.typeDisplay = '[color=ff00ff]' + str(ont.at[row, 'machineType']) + '[/color]'

                    # Fetching information and storing as variable to send to GUI #
                    if str(ont.at[row, 'RMA?']) == '' or '0':
                        checkStatus.rma = '[color=ff00ff]0[/color]'
                    elif str(ont.at[row, 'RMA?']) != '':
                        checkStatus.rma = '[color=ff00ff]' + str(ont.at[row, 'RMA?']) + '[/color]'
                    checkStatus.serial = str(ont.at[row, 'serialNumber']) # No markup since its being sent to the 'serial' text input for copy/paste
                    checkStatus.type = '[color=ff00ff]' + str(ont.at[row, 'machineType']) + '[/color]'
                    checkStatus.expiry = '[color=ff00ff]' + str(ont.at[row, 'expiryDate']) + '[/color]'
                    checkStatus.psu = '[color=ff00ff]' + str(ont.at[row, 'PSU SN']) + '[/color]'
                    checkStatus.speed = '[color=ff00ff]' + str(ont.at[row, 'BaseSpeed']) + '[/color]'
                    checkStatus.ip = '[color=ff00ff]' + str(ont.at[row, 'IP Address']) + '[/color]'
                    checkStatus.status = '[color=ff00ff]' + str(ont.at[row, 'Status']) + '[/color]'
                    checkStatus.deploydate = '[color=ff00ff]' + str(ont.at[row, 'deployDate']) + '[/color]'
                    checkStatus.acc = '[color=ff00ff]' + str(ont.at[row, 'accountName']) + '[/color]'
                    checkStatus.host = '[color=ff00ff]' + str(ont.at[row, 'Hosted?']) + '[/color]'
                    checkStatus.coin = '[color=ff00ff]' + str(ont.at[row, 'coinMined']) + '[/color]'
                    checkStatus.pool = '[color=ff00ff]' + str(ont.at[row, 'pool']) + '[/color]'
                    checkStatus.nosdate = ''
                    checkStatus.nos = ''
                    checkStatus.scanned = True
                    return

        if scan not in onlineIp:
            checkStatus.ip = ''
            checkStatus.type = ''
            checkStatus.status = ''
            checkStatus.deploydate = ''
            checkStatus.acc = ''
            checkStatus.host = ''
            checkStatus.coin = ''
            checkStatus.pool = ''
            checkStatus.nosdate = ''
            checkStatus.nos = ''
            checkStatus.hashrate = ''
            checkStatus.uptime = ''
            updater().invalidIP()
            return

    def verify(self):

        if checkStatus.scanned is True:
            self.manager.get_screen('editInfo').ids.rma.text = checkStatus.rma.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.serial.text = checkStatus.serial
            self.manager.get_screen('editInfo').ids.machinetype.text = checkStatus.type.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.account.text = checkStatus.acc.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.expiry.text = checkStatus.expiry.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.deploy.text = checkStatus.deploydate.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.status.text = checkStatus.status.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.hosted.text = checkStatus.host.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.coin.text = checkStatus.coin.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.pool.text = checkStatus.pool.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.psu.text = checkStatus.psu.split('ff]')[1].split('[/')[0]
            self.manager.get_screen('editInfo').ids.speed.text = checkStatus.speed.split('ff]')[1].split('[/')[0]
            self.manager.current = 'editInfo'
        else:
            # Popup saying to scan a machine before trying to edit
            updater().scanfirst()
        # checkStatus.scanned = False


class editInfo(Screen):

    def update(self, row, rma, serial, machinetype, account, expiry, deploy, status, hosted, coin, pool, psu, speed, user):
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")
        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt1 = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)
        offt = offt1.replace(numpy.nan, '', regex=True)
        localList()
        changes = []

        if ont.at[row, 'RMA?'] == rma:
            pass
        else:
            ont.at[row, 'RMA?'] = rma
            # changes.append('RMA')

        if ont.at[row, 'serialNumber'] == serial:
            pass
        else:
            ont.at[row, 'serialNumber'] = serial
            changes.append('serialNumber')

        if ont.at[row, 'machineType'] == machinetype:
            pass
        else:
            ont.at[row, 'machineType'] = machinetype
            changes.append('machineType')

        if ont.at[row, 'accountName'] == account:
            pass
        else:
            ont.at[row, 'accountName'] = account
            changes.append('accountName')

        if ont.at[row, 'expiryDate'] == expiry:
            pass
        else:
            ont.at[row, 'expiryDate'] = expiry
            changes.append('expiryDate')

        if ont.at[row, 'deployDate'] == deploy:
            pass
        else:
            ont.at[row, 'deployDate'] = deploy
            changes.append('deployDate')

        if ont.at[row, 'Status'] == status:
            pass
        else:
            ont.at[row, 'Status'] = status
            changes.append('Status')

        if ont.at[row, 'Hosted?'] == hosted:
            pass
        else:
            ont.at[row, 'Hosted?'] = hosted
            changes.append('Hosted?')

        if ont.at[row, 'coinMined'] == coin:
            pass
        else:
            ont.at[row, 'coinMined'] = coin
            changes.append('coinMined')

        if ont.at[row, 'pool'] == pool:
            pass
        else:
            ont.at[row, 'pool'] = pool
            changes.append('pool')

        if ont.at[row, 'PSU SN'] == psu:
            pass
        else:
            ont.at[row, 'PSU SN'] = psu
            changes.append('PSU SN')

        if ont.at[row, 'BaseSpeed'] == speed:
            pass
        else:
            ont.at[row, 'BaseSpeed'] = speed
            changes.append('BaseSpeed')

        ont.at[row, 'Notes'] = 'Edited ' + z + ' - ' + changes[0] + ' - ' + user
        ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
        return


class CheckoutScreen(Screen):
    pass


class CheckinScreen(Screen):
    openips = ''
    def random(self):
        CheckinScreen.openips = str(len(openIp))
        return
    pass


class machineRepair(Screen):
    def repair(self, scan):
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If serial field is empty #
        if scan == "":
            updater().alert3()
            return

        # If serial is on offline tracker #
        if scan in offlineSerial:
            updater().alert1()
            return

        # If serial is not in online tracker #
        if scan not in onlineSerial:
            updater().alert2()
            return

        # Starting count to locate which row the machine is in within the tracker #
        row = -1
        for x in onlineSerial:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                if ont.at[row, 'Status'] != 'On Shelf':
                    updater().notDeployed()
                    return

                # Creating info for where the machine is located in regards to ontracker sheet #
                infoList = ont.iloc[row]
                newRow = offt.iloc[:, :]
                appended = newRow.append(infoList)

                # Clearing machines info from online tracker, leaving IP Address w/ NOS - Empty status #
                ont.at[row, 6:] = ''
                ont.at[row, 'Status'] = 'NOS - Empty'
                ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                appended.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
                offt.at[(len(offt['IP Address']) - 1), 'NOS Date'] = z
                offt.at[(len(offt['IP Address']) - 1), 'NOS Reason'] = 'REPAIR'

                # Add users name to new column to track who checked out what machine #
                offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.ids.userList.text
                offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nchecked out for repair[/color]"
                self.ids.serial.text = ''
                localList()
                return
            else:
                return
        return

    def refocus(self):
        keyboard.press_and_release('enter')
        return


class machineCOW(Screen):

    # Function to move a machine from Online Tracker to Offline Tracker with COW status #

    def updateCOW(self, scan):
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        serialList = []
        row = -1

        for serial in ont['serialNumber']:
            if scan == serial:
                row += 1
                if '9' in ont.at[row, 'machineType']:
                    if ont.at[row, 'Status'] != 'On Shelf':
                        updater().notDeployed()
                        return

                    # Creating info for where the machine is located in regards to ontracker sheet #
                    infoList = ont.iloc[row]
                    newRow = offt.iloc[:, :]
                    appended = newRow.append(infoList)

                    # Clearing machines info from online tracker, leaving IP Address w/ NOS - Empty status #
                    ont.at[row, 6:] = ''
                    ont.at[row, 'Status'] = 'NOS - Empty'
                    ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                    appended.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                    offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
                    offt.at[(len(offt['IP Address']) - 1), 'NOS Date'] = z
                    offt.at[(len(offt['IP Address']) - 1), 'NOS Reason'] = 'COW'

                    # Add users name to new column to track who checked out what machine #
                    offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.ids.userList.text
                    offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                    self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been COW'd[/color]"
                    self.ids.serial.text = ''
                    localList()
                    return
            else:
                row += 1
        print('machine not found in online tracker, checking offline tracker...')

        for serial in offt['serialNumber']:
            serialList.append(serial)

        row = 0
        for serial in reversed(serialList):
            if scan == serial:
                row += 1
                machine = len(offt['serialNumber']) - row
                if offt.at[machine, 'NOS Reason'] == 'REPAIR':
                    offt.at[machine, 'NOS Date'] = z
                    offt.at[machine, 'NOS Reason'] = 'COW'
                    offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.manager.get_screen('Menu').ids.userList.text

                    self.ids.label.text = '[color=00ff00]' + offt.at[machine, 'IP Address'] + "\nhas been COW'd[/color]"
                    self.ids.serial.text = ''
                    offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False)
                    return

                elif offt.at[machine, 'NOS Reason'] != 'REPAIR':
                    print('Machine not checked-out for repairs...')
                    return
            else:
                row += 1
        if scan not in offt['serialNumber']:
            print('serial not listed in offline tracker...')
        return

    # Literally all this function does is simulate the user pressing the enter key #
    def refocus(self):
        keyboard.press_and_release('enter')
        return


class RMA(Screen):
    row = 0

    # Function to move machine from online tracker to offline tracker with RMA status #
    def machineRMA(self, scan):
        scan = scan.upper()

        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt1 = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)
        offt = offt1.replace(numpy.nan, '', regex=True)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If the serial field is empty #
        if scan == "end" or scan == "":
            updater().alert3()
            return

        # If the serial number is already in the offline tracker #
        # if scan in offlineSerial:
        #     updater().alert1()
        #     return

        # Checking to see if the serial number is in the online tracker #
        if scan not in onlineSerial:
            updater().alert2()
            return

        # Starting count to locate which row the machine is in within the tracker #
        row = -1
        for x in onlineSerial:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                if ont.at[row, 'Status'] != 'On Shelf':
                    updater().notDeployed()
                    return

                else:
                    # Creating info for where the machine is located in regards to online tracker #
                    infoList = ont.iloc[row]

                    infoList['RMA?'] = (int(infoList['RMA?']) + 1)

                    newRow = offt.iloc[:, :]
                    appended = newRow.append(infoList)

                    # Increase RMA count before removing info #
                    offt.at[len(offt['IP Address']) - 1, 'RMA?'] = (int(infoList['RMA?']) + 1)
                    ont.at[row, 6:] = ''
                    ont.at[row, 'Status'] = 'NOS - Empty'

                    ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                    appended.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                    offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)

                    # Add users name to new column to track who RMA'd what machine #
                    offt.at[(len(offt['IP Address']) - 1), 'NOS Date'] = z
                    offt.at[(len(offt['IP Address']) - 1), 'NOS Reason'] = 'RMA'
                    offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.manager.get_screen('Menu').ids.userList.text
                    offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                    self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been RMA'd[/color]"
                    self.ids.serial.text = ''
                    localList()
                    return
            else:
                return
        return

    # Function to move machine from online tracker to offline tracker with RMA status #
    def psuRMA(self, scan):
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        psut = pd.read_csv(r'//10.20.0.25/Documents/offline PSU tracker.csv', dtype=object)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If the serial field is empty #
        if scan == "end" or scan == "":
            updater().alert3()
            return

        # Checking to see if the serial number is in the online tracker #
        if scan not in onlinePSU:
            updater().alert2()
            return

        # Starting count to locate which row the machine is in within the tracker #
        row = -1
        for x in ont['PSU SN']:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1
                RMA.row = row

                if ont.at[row, 'Status'] != 'On Shelf':
                    updater().notDeployed()
                    return

                else:
                    # Creating info for where the machine is located in regards to online tracker #
                    infoList = ont.iloc[row]
                    ipaddress = infoList[0]
                    machinetype = infoList[7]
                    psuserial = infoList[16]
                    newRow = psut.iloc[:, :]
                    appended = newRow.append(pd.Series(), ignore_index=True)

                    # Create pop-up to scan new PSU serial number #
                    updater().newpsu()

                    # Saving trackers and appending Date/Name to offline tracker entry #
                    appended.to_csv(r'//10.20.0.25/Documents/offline PSU tracker.csv', index=False, encoding='utf8')
                    psut = pd.read_csv(r'//10.20.0.25/Documents/offline PSU tracker.csv', dtype=object)
                    psut.at[(len(psut['serialNumber']) - 1), 'serialNumber'] = psuserial
                    psut.at[(len(psut['serialNumber']) - 1), 'NOS Date'] = z
                    psut.at[(len(psut['serialNumber']) - 1), 'NOS Reason'] = 'RMA'
                    psut.at[(len(psut['serialNumber']) - 1), 'Name'] = self.manager.get_screen('Menu').ids.userList.text
                    psut.to_csv(r'//10.20.0.25/Documents/offline PSU tracker.csv', index=False, encoding='utf8')
                    self.ids.label.text = "[color=00ff00]RMA'd " + onlineIp[row] + "'s\nPSU[/color]"
                    self.ids.serial.text = ''
                    localList()
                    return
            else:
                return
        return

    def updateLabel(self, result):
        if result is True:
            self.ids.label.text = "[color=00ff00]RMA'd PSU Successfully[/color]"
        elif result is False:
            print(result)
            self.ids.label.text = '[color=00ff00]Failed to RMA PSU[/color]'
        return

    def checkToggle(self, scan, machinestate, psustate):
        # scan = self.manager.get_screen('RMA').ids.serial.text
        if machinestate == 'down' and psustate == 'normal':
            RMA.machineRMA(self, scan)
        elif psustate == 'down' and machinestate == 'normal':
            RMA.psuRMA(self, scan)

    # Literally all this function does is simulate the user pressing the enter key #
    def refocus(self):
        keyboard.press_and_release('enter')
        return


class machineHOS(Screen):

    # Function to move a machine from Online Tracker to Offline Tracker with HOS status #
    def updateHOS(self, scan):
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If serial field is empty #
        if scan == "end" or scan == "":
            updater().alert3()
            return

        # If serial number is already in the offline tracker #
        if scan in offlineSerial:
            updater().alert1()
            return

        # Checking that the serial number is actually in the online tracker #
        if scan not in onlineSerial:
            updater().alert2()
            return

        # Starting count to locate the row the machine is in within the tracker #
        row = -1
        for x in onlineSerial:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                if ont.at[row, 'Status'] != 'On Shelf':
                    updater().notDeployed()
                    return

                # Storing machine info, setting variable to new row in offline tracker #
                infoList = ont.iloc[row]
                newRow = offt.iloc[:, :]
                appended = newRow.append(infoList)

                # Clearing machines info from online tracker, leaving IP Address w/ NOS - Empty status #
                ont.at[row, 6:] = ''
                ont.at[row, 'Status'] = 'NOS - Empty'
                ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                appended.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
                offt.at[(len(offt['IP Address']) - 1), 'NOS Date'] = z
                offt.at[(len(offt['IP Address']) - 1), 'NOS Reason'] = 'HOS'

                # Add users name to new column to track who moved what machine to the hospital #
                offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.manager.get_screen('Menu').ids.userList.text
                offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been moved\nto hospital[/color]"
                self.ids.serial.text = ''
                localList()
                return
            else:
                return
        return

    # Literally all this function does is simulate the user pressing the enter key #
    def refocus(self):
        keyboard.press_and_release('enter')
        return


class cleaningIn(Screen):

    def cleaningScan(self, scan):
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        localList()

        x = datetime.datetime.now()
        z = x.strftime("%m.%d.%Y")

        # If serial field is empty #
        if scan == "end" or scan == "":
            updater().alert3()
            return

        # Scanning serial numbers in ontracker for scanned machine #
        if scan not in onlineSerial:
            updater().alert2()
            return

        # Starting count to locate which row the serial number is in within the tracker #
        row = -1
        for x in onlineSerial:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                # Checking machine status, if status is 'NOS - Cleaning', it gets changed to 'On Shelf' #
                # If scanned machine is not listed as off-shelf for cleaning, popup occurs #
                if 'NOS - Cleaning' in ont.at[row, 'Status']:
                    ont.at[row, 'Status'] = 'On Shelf'
                else:
                    updater().no4c()
                    return

                ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                log = open(r'cleaningIn/' + z + '.txt', 'a')
                log.write(onlineIp[row] + ' checked in | ' + self.manager.get_screen('Menu').ids.userList.text + '\n')
                log.close()
                self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been checked-in\nfrom cleaning[/color]"
                self.ids.serial.text = ''
                localList()
                return
            else:
                return
        return

    def cleaningScanIP(self, ip):
        scan = ip
        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        localList()

        x = datetime.datetime.now()
        z = x.strftime("%m.%d.%Y")

        # If serial field is empty #
        if scan == "end" or scan == "":
            updater().alert3()
            return

        if '10.10.' not in scan:
            scan = '10.10.' + scan

        # Scanning serial numbers in ontracker for scanned machine #
        if scan not in onlineIp:
            updater().alert2()
            return

        # Starting count to locate which row the serial number is in within the tracker #
        row = -1
        for x in onlineIp:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                # Checking machine status, if status is 'NOS - Cleaning', it gets changed to 'On Shelf' #
                # If scanned machine is not listed as off-shelf for cleaning, popup occurs #
                if 'NOS - Cleaning' in ont.at[row, 'Status']:
                    ont.at[row, 'Status'] = 'On Shelf'
                else:
                    updater().no4c()
                    return

                ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                log = open(r'cleaningIn/' + z + '.txt', 'a')
                log.write(onlineIp[row] + ' checked in | ' + self.manager.get_screen('Menu').ids.userList.text + '\n')
                log.close()
                self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been checked-in\nfrom cleaning[/color]"
                self.ids.serial.text = ''
                localList()
                return
            else:
                return
        return

    # Literally all this function does is simulate the user pressing the enter key #
    def refocus(self):
        keyboard.press_and_release('enter')
        return


class CheckinBulk(Screen):
    skippedMachines = ''
    numMachines = ''

    def rangeIn(self, ipblock, ipstart, ipend):
        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)

        skipped = 0
        num = 0

        # Lists for creating restrictions on Rows & IPs #
        ipRange = []
        specRows = [111, 125, 126, 149, 150]
        allowedRow = []
        allowedIP = list(range(2, 217))
        allowedIP.append(254)

        for i in range(11, 37):
            allowedRow.append(i)
        for i in range(39, 69):
            allowedRow.append(i)
        for i in specRows:
            allowedRow.append(i)

        # Checking that the row entered is valid #
        if int(ipblock.text) not in allowedRow:
            # create alert for invalid row
            updater().badRow()
            return

        # Checking that the starting IP is valid #
        if int(ipstart.text) not in allowedIP:
            updater().errorRange()
            return

        # Creating our starting IP based on what the user enters #
        ipblock = '10.10.' + ipblock.text
        ipactualstart = ipblock + '.' + ipstart.text
        ipfalsestart = ipstart.text

        # If IP end is blank #
        if str(ipend.text) == '':
            ipend.text = ipstart.text
            ont.at[onlineIp.index(ipactualstart), 'Status'] = 'On Shelf'
            ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')

            skipped = str(skipped)
            num = str(num + 1)
            CheckinBulk.skippedMachines = str(skipped)
            CheckinBulk.numMachines = str(num)
            updater().conf()

            ipstart.text = ''
            ipend.text = ''
            return

        # If IP end is 254 #
        elif int(ipend.text) == 254:
            updater().error254()
            return

        # If IP end falls out of range #
        elif int(ipend.text) not in allowedIP:
            updater().errorRange()
            return

        # If IP start is 254 #
        if int(ipstart.text) == 254:
            ipRange.append(ipblock + '.' + ipstart.text)
            ipfalsestart = '2'

        # If IP start falls out of range #
        elif int(ipstart.text) not in allowedIP:
            updater().errorRange()
            return

        # Taking the IP block and IP start/end to start creating our 'range' #
        firstIp = ipblock + '.' + ipfalsestart
        lastIp = ipblock + '.' + ipend.text

        # Selecting just the last numbers from the IP addresses for comparing #
        firstSplit = int(firstIp.split('.')[3])
        lastSplit = int(lastIp.split('.')[3])

        # If the starting IP is bigger than the end #
        if firstSplit > lastSplit:
            updater().errorRange()
            return

        # Comparing start and end IP and incrementing the starting IP by 1 until it is no longer less than the end #
        # Obviously the new incremented IP's are added to our IP range list we made at the beginning #

        for i in range(firstSplit, lastSplit):
            if firstSplit < lastSplit:
                ipRange.append(ipblock + '.' + str(firstSplit))
                firstSplit += 1

        startingRow = onlineIp.index(firstIp)
        endingRow = onlineIp.index(lastIp) + 1

        # Creating a range of cells in the tracker using our starting and ending IP's #
        cell_range = ont.iloc[startingRow:endingRow, 11].values

        # Special exception for the starting IP being the first machine, 254 #
        if int(ipstart.text) == 254:
            ont.at[onlineIp.index(ipactualstart), 'Status'] = 'On Shelf'

            #ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
            ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
            print('254')

        # Iterating through range of cells and counting the number of status's that say 'On Shelf' #
        # If status says anything other than 'On Shelf' it gets skipped, and is counted as a skipped machine #
        row = startingRow
        for cell in cell_range:
            if 'NOS - Cleaning' in ont.at[row, 'Status']:
                ont.at[row, 'Status'] = 'On Shelf'
                num += 1
                row += 1
            else:
                skipped += 1
                row += 1

        # Updating machine status' to show they are off shelf for cleaning #
        ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')

        # Passing values to kv file to display machine count #
        skipped = str(skipped)
        num = str(num)
        CheckinBulk.skippedMachines = str(skipped)
        CheckinBulk.numMachines = str(num)
        updater().conf2()
        localList()
        ipstart.text = ''
        ipend.text = ''


class cleaningOut(Screen):

    skippedMachines = ''
    numMachines = ''

    def rangeOut(self, ipblock, ipstart, ipend):
        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)

        x = datetime.datetime.now()
        z = x.strftime("%m-%d-%Y")

        skipped = 0
        num = 0

        # Lists for creating restrictions on Rows & IPs #
        ipRange = []
        specRows = [111, 125, 126, 149, 150]
        allowedRow = []
        allowedIP = list(range(2,217))
        allowedIP.append(254)

        for i in range(11,37):
            allowedRow.append(i)
        for i in range(39, 69):
            allowedRow.append(i)
        for i in specRows:
            allowedRow.append(i)

        # Checking that the row entered is valid #
        if int(ipblock.text) not in allowedRow:
            updater().badRow()
            return

        # Checking that the starting IP is valid #
        if int(ipstart.text) not in allowedIP:
            updater().errorRange()
            return

        # Creating our starting IP based on what the user enters #
        ipblock = '10.10.' + ipblock.text
        ipactualstart = ipblock + '.' + ipstart.text
        ipfalsestart = ipstart.text

        # If IP end is blank #
        if str(ipend.text) == '':
            ipend.text = ipstart.text
            ont.at[onlineIp.index(ipactualstart), 'Status'] = 'NOS - Cleaning'
            ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')

            skipped = str(skipped)
            num = str(num + 1)
            cleaningOut.skippedMachines = str(skipped)
            cleaningOut.numMachines = str(num)
            updater().conf()

            ipstart.text = ''
            ipend.text = ''
            return

        # If IP end is 254 #
        elif int(ipend.text) == 254:
            updater().error254()
            return

        # If IP end falls out of range #
        elif int(ipend.text) not in allowedIP:
            updater().errorRange()
            return

        # If IP start is 254 #
        if int(ipstart.text) == 254:
            ipRange.append(ipblock + '.' + ipstart.text)
            ipfalsestart = '2'

        # If IP start falls out of range #
        elif int(ipstart.text) not in allowedIP:
            updater().errorRange()
            return

        # Taking the IP block and IP start/end to start creating our 'range' #
        firstIp = ipblock + '.' + ipfalsestart
        lastIp = ipblock + '.' + ipend.text

        # Selecting just the last numbers from the IP addresses for comparing #
        firstSplit = int(firstIp.split('.')[3])
        lastSplit = int(lastIp.split('.')[3])

        # If the starting IP is bigger than the end #
        if firstSplit > lastSplit:
            updater().errorRange()
            return

        # Comparing start and end IP and incrementing the starting IP by 1 until it is no longer less than the end #
        # Obviously the new incremented IP's are added to our IP range list we made at the beginning #

        for i in range(firstSplit, lastSplit):
            if firstSplit < lastSplit:
                ipRange.append(ipblock + '.' + str(firstSplit))
                firstSplit += 1

        startingRow = onlineIp.index(firstIp)
        endingRow = onlineIp.index(lastIp) + 1

        # Creating a range of cells in the tracker using our starting and ending IP's #
        cell_range = ont.iloc[startingRow:endingRow, 11].values

        # Special exception for the starting IP being the first machine, 254 #
        if int(ipstart.text) == 254:
            ont.at[onlineIp.index(ipactualstart), 'Status'] = 'NOS - Cleaning'

            ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
            ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)

        # Iterating through range of cells and counting the number of status's that say 'On Shelf' #
        # If status says anything other than 'On Shelf' it gets skipped, and is counted as a skipped machine #
        row = startingRow
        for cell in cell_range:
            if 'On Shelf' in ont.at[row, 'Status']:
                ont.at[row, 'Status'] = 'NOS - Cleaning'
                num += 1
                row += 1
            else:
                skipped += 1
                row += 1

        # Updating machine status' to show they are off shelf for cleaning #
        # Make this change each status individually rather than a whole section (with a for loop or something) #
        ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
        log = open(r'cleaningOut/cleaningOut.txt', 'a')
        log.write(firstIp + ' - ' + ipend.text + ' out for cleaning | ' + self.manager.get_screen('Menu').ids.userList.text + ' | ' + z + '\n')
        log.close()
        # Passing values to kv file to display machine count #
        skipped = str(skipped)
        num = str(num + 1)
        cleaningOut.skippedMachines = str(skipped)
        cleaningOut.numMachines = str(num)
        updater().conf()
        localList()
        ipstart.text = ''
        ipend.text = ''


class newMinerScreen(Screen):

    settings = {}
    machineType = ''
    account = ''
    hosted = ''
    coin = ''
    pool = ''
    expiration = ''
    speed = ''
    deployedSerial = ''
    ip = ''
    rma = ''

    def getSettings(self):

        # Reading settings from text file and storing them as list #
        settingstext = open(r'machinesettings.txt', 'r')
        for i in settingstext:
            option, value = i.strip().split(':', 1)
            newMinerScreen.settings[option] = value.strip()

        # Taking info from list and adding it to dictionary for easier calling #
        newMinerScreen.machineType = newMinerScreen.settings['Machine Type']
        newMinerScreen.speed = newMinerScreen.settings['Speed']
        newMinerScreen.account = newMinerScreen.settings['Account Name']
        newMinerScreen.hosted = newMinerScreen.settings['Hosted']
        newMinerScreen.coin = newMinerScreen.settings['Coin Mined']
        newMinerScreen.pool = newMinerScreen.settings['Pool']
        newMinerScreen.expiration = newMinerScreen.settings['Expiration']
        newMinerScreen.rma = newMinerScreen.settings['RMA']
        return

    def deployNew(self, psu, serial, ip, machineType, speed, account, hosted, coin, pool):
        newMinerScreen.ip = ip

        machineType = newMinerScreen.settings['Machine Type']
        speed = newMinerScreen.settings['Speed']
        account = newMinerScreen.settings['Account Name']
        hosted = newMinerScreen.settings['Hosted']
        coin = newMinerScreen.settings['Coin Mined']
        pool = newMinerScreen.settings['Pool']
        expiration = newMinerScreen.settings['Expiration']
        rma = newMinerScreen.settings['RMA']

        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)

        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If IP field is empty #
        if ip == '':
            updater().noip()
            return

        # If no PSU SN is scanned #
        if psu == '':
            updater().nopsu()
            return

        # If serial field is empty #
        if serial == '':
            updater().alert3()
            return

        # If the same serial number is scanned for PSU and Machine #
        if psu == serial:
            print('PSU SN and Machine SN are the same!')
            return

        # If PSU serial number is already listed in the online tracker #
        if psu in ont['PSU SN']:
            print('PSU already exists in tracker!')
            return

        # If no speed is entered #
        if speed == '':
            updater().nospeed()
            return

        # If a 'new' machines SN is in offline tracker #
        if serial in offlineSerial:
            updater().alert1()
            return

        row = 0
        for i in ont['serialNumber']:
            if serial == i:
                row += 1
                newMinerScreen.ip = ont.at[row, 'IP Address']
                updater().snExists(newMinerScreen.ip)
                return

        # If IP is not valid #
        if ip not in onlineIp:
            updater().invalidIP()
            return

        count = -1

        for i in ont['IP Address']:
            if i != ip:
                count += 1

            elif i == ip:
                count += 1

                # If scanned serial number is already deployed #
                if serial in onlineSerial:
                    print(str(ont.at[count, 'serialNumber']))
                    updater().alreadyDeployed()
                    return

                # Updating cell values to store new machines info #
                elif ont.at[count, 'serialNumber'] == '':
                    if ont.at[count, 'Status'] == 'NOS - Empty':
                        ont.at[count, 'PSU SN'] = psu.upper()
                        ont.at[count, 'serialNumber'] = serial.upper()
                        ont.at[count, 'BaseSpeed'] = speed
                        ont.at[count, 'machineType'] = machineType
                        ont.at[count, 'accountName'] = account
                        ont.at[count, 'deployDate'] = z
                        ont.at[count, 'Status'] = 'On Shelf'
                        ont.at[count, 'Hosted?'] = hosted
                        ont.at[count, 'RMA?'] = rma
                        ont.at[count, 'coinMined'] = coin
                        ont.at[count, 'pool'] = pool
                        ont.at[count, 'expiryDate'] = expiration
                    elif ont.at[count, 'Status'] == 'NOS - Power':

                        row = 0
                        status = False
                        nextip = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
                        for i in ont['IP Address']:
                            if i == nextip:
                                row += 1
                                if ont.at[row, 'Status'] == 'NOS - Power':
                                    nextip = '10.10.' + nextip.split('.')[2] + '.' + str(
                                        int(nextip.split('.')[3]) + 1)
                                elif ont.at[row, 'Status'] != 'NOS - Power':
                                    self.ids.ip.text = '10.10.' + nextip.split('.')[2] + '.' + str(
                                        int(nextip.split('.')[3]) + 1)
                                    status = True
                            else:
                                row += 1
                        updater().nospower()
                        return
                    elif ont.at[count, 'Status'] != 'NOS - Empty':
                        updater().statuserror()
                        return

                    ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                    self.ids.label.text = '[color=00ff00]' + ont.at[count, 'IP Address'] + ' has been deployed[/color]'

                    if int(ip.split('.')[3]) == 254:
                        self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.2'
                        return
                    elif int(ip.split('.')[3]) == 216:
                        self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.254'
                        return
                    elif int(ip.split('.')[3]) < 217:
                        self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
                        row = 0
                        status = False
                        nextip = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]))
                        for i in ont['IP Address']:
                            if i == nextip:
                                row += 1
                                if ont.at[row, 'Status'] == 'NOS - Power':
                                    nextip = '10.10.' + nextip.split('.')[2] + '.' + str(
                                        int(nextip.split('.')[3]) + 1)
                                elif ont.at[row, 'Status'] != 'NOS - Power':
                                    self.ids.ip.text = '10.10.' + nextip.split('.')[2] + '.' + str(
                                        int(nextip.split('.')[3]) + 1)
                                    status = True
                            else:
                                row += 1
                        return
                    return

                # If cell for serial number at the defined IP is not blank (a machine is already there)#
                elif ont1.at[count, 'serialNumber'] != '':
                    newMinerScreen.deployedSerial = ont1.at[count, 'serialNumber']
                    updater().alreadyDeployed()
                    return

        # If IP is 254, set next one to 2 #
        row = 0
        if int(ip.split('.')[3]) == 254:
            self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.2'
            return
        elif int(ip.split('.')[3]) == 216:
            self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.254'
            return
        elif int(ip.split('.')[3]) < 217:
            nextip = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
            self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
            status = False
            for i in ont['IP Address']:
                if i == nextip:
                    row += 1
                    while status == False:
                        if ont.at[row, 'Status'] == 'NOS - Power':
                            self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
                            print('skipped: power')
                        elif ont.at[row, 'Status'] != 'NOS - Power':
                            self.ids.ip.text = '10.10.' + ip.split('.')[2] + '.' + str(int(ip.split('.')[3]) + 1)
                            status = True
                    return
                else:
                    row += 1
            return



class newminerSettings(Screen):
    pass


class redeployScreen(Screen):
    pass


class redeployScreen2(Screen):

    ip = ''
    type = ''
    nos = ''
    nosdate = ''
    occupied = ''

    ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
    offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)

    def redeployCOW(self, scan, newIP):
        scan = scan.upper()

        ont1 = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt1 = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        ont = ont1.replace(numpy.nan, '', regex=True)
        offt = offt1.replace(numpy.nan, '', regex=True)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If serial field is empty #
        if scan == "":
            updater().alert3()
            return

        # If serial is on online tracker #
        row = 0
        for i in ont['serialNumber']:
            if scan == i.upper():
                redeployScreen2.occupied = ont.at[row, 'IP Address']
                updater().snExists()
                return
            else:
                row += 1

        # If serial is not in offline tracker #
        if scan not in offlineSerial:
            updater().alert2()
            return

        # Starting count to locate which row the machine is in within the tracker #
        row = -1
        newRow = -1

        for i in ont.iloc[:, 0]:
            if str(newIP) != i:
                newRow += 1
            elif str(newIP) == i:
                newRow += 1

                if ont.iat[newRow, 6] != '':
                    print('Serial not empty')
                    return

                for x in offt['serialNumber']:
                    if scan != x:
                        row += 1
                    elif scan == x:
                        row += 1

                        # Saving IP info from Online Tracker #
                        ipInfo = []
                        for i in ont.iloc[newRow, 0:6]:
                            ipInfo.append(i)

                        # Copying machine info from offline to online tracker #
                        ont.iloc[newRow] = offt.iloc[row]

                        # Clearing machines info from online tracker, leaving IP Address w/ NOS - Empty status #
                        ont.at[newRow, 'Status'] = 'On Shelf'
                        ont.at[newRow, 0:6] = ipInfo

                        if 'RMA' in offt.at[row, 'NOS Reason']:
                            pass
                        elif 'RMA' not in offt.at[row, 'NOS Reason']:
                            offt = offt.drop(row)
                            offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                        ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                        self.ids.label.text = '[size=45][color=00ff00]' + newIP + "\nhas been re-deployed[/color][/size]"
                        self.ids.serial.text = ''
                        localList()
                        return
                    else:
                        return
        return


class exit(Screen):

    # Function to move a machine from Online Tracker to Offline Tracker with COW status #
    def exit(self, scan):
        scan = scan.upper()

        ont = pd.read_csv(r'//10.20.0.25/Documents/tracker.csv', dtype=object)
        offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
        localList()

        # Storing current date for COW sheet #
        x = datetime.datetime.now()
        z = x.strftime("%m/%d/%Y")

        # If serial input is empty #
        if scan == "":
            updater().alert3()
            return

        # If serial number is already in offline tracker #
        if scan in offlineSerial:
            updater().alert3()

        # Scanning serial numbers in ontracker for scanned machine #
        if scan not in onlineSerial:
            updater().alert2()
            return

        # Starting a counter in order to locate which row the scanned machine is in within the tracker #
        # Starting at -1 for stupid indexing purposes, saves from having to subtract 1 every time we reference #
        row = -1
        for x in onlineSerial:
            if scan != x:
                row += 1
            elif scan == x:
                row += 1

                if ont.at[row, 'Status'] != 'On Shelf':
                    updater().notDeployed()
                    return

                # Creating info for where the machine is located in regards to ontracker sheet #
                infoList = ont.iloc[row]
                newRow = offt.iloc[:, :]
                appended = newRow.append(infoList)

                # Clearing machines info from online tracker, leaving IP Address w/ NOS - Empty status #
                ont.at[row, 6:] = ''
                ont.at[row, 'Status'] = 'NOS - Empty'
                ont.to_csv(r'//10.20.0.25/Documents/tracker.csv', index=False, encoding='utf8')
                appended.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                offt = pd.read_csv(r'//10.20.0.25/Documents/offline tracker.csv', dtype=object)
                offt.at[(len(offt['IP Address']) - 1), 'NOS Date'] = z
                offt.at[(len(offt['IP Address']) - 1), 'NOS Reason'] = 'INVENTORY'
                offt.at[(len(offt['IP Address']) - 1), 'Name'] = self.manager.get_screen('Menu').ids.userList.text
                offt.to_csv(r'//10.20.0.25/Documents/offline tracker.csv', index=False, encoding='utf8')
                self.ids.label.text = '[color=00ff00]' + onlineIp[row] + "\nhas been moved\nto inventory[/color]"
                self.ids.serial.text = ''
                localList()
                return
            else:
                return
        return

    def refocus(self):
        keyboard.press_and_release('enter')
        return


# Building the GUI #
class TestApp(App):
    localList()
    # Variables used to pass info between classes #
    func = updater()
    cow = machineCOW()
    rma = RMA()
    rd = redeployScreen2()
    nm = newMinerScreen()
    nms = newminerSettings()
    cleanOut = cleaningOut()
    cleanIn = cleaningIn()
    refocus = WindowManager()
    CheckinBulk = CheckinBulk()

    def build(self):
        self.title= 'Tracker Updater V1.5'
        return Builder.load_file("gui.kv")


# Launching App #
if __name__ == "__main__":
    TestApp().run()
